package com.devcamp.s50.task5820.restapi.controller;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5820.restapi.model.CDrink;
import com.devcamp.s50.task5820.restapi.respository.IDrinkRepository;
@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository pCustomeRepository;
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks(){
        try{
            List<CDrink> listCustomer = new ArrayList<CDrink>();
            pCustomeRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
